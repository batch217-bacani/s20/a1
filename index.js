//console.log("Hello World!");
// 1.

let userInput = Number(prompt("Input a number"));
console.log("The number you provided is " + userInput + ".");
for (userInput; userInput >= 0 ; userInput--) {
	if (userInput <= 50) {
		console.log("The current value is at " + userInput + ". Terminating the loop.");
		break;
	}
	if (userInput % 10 == 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue;
	}
	if (userInput % 5 == 0) {
		console.log(userInput);
	}
}


// 2.

let word = "supercalifragilisticexpialidocious";
let consonants = "";

console.log(word);
for (let i = 0; word.length > i; i++) {
	if (
		word[i] == "a" ||
		word[i] == "e" ||
		word[i] == "i" ||
		word[i] == "o" ||
		word[i] == "u"
		) {
		continue;
	}
	else {
		consonants = consonants + word[i];
	}
}
console.log(consonants);